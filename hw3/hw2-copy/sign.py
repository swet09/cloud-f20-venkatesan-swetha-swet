#this python files calls sign.html file into it 
from flask import redirect, request, url_for, render_template
from flask.views import MethodView
from datetime import datetime
import gbmodel

class Sign(MethodView):
    def get(self):
        return render_template('sign.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = gbmodel.get_model()
        model.insert(request.form['Name'], request.form['Description'], request.form['Street_Address'], request.form['Type_Of_Service'], request.form['Phone_Number'], request.form['Hours_Of_Operation'], request.form['Reviews'])
        return redirect(url_for('index'))
