class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, Name, Description, Street_Address, Type_Of_Service, Phone_Number, Hours_Of_Operation, Reviews):
        """
        Inserts entry into database
        :param Name: String
        :param Description: String
        :param Street_Address: String
        :Param Type_Of_Service: string
        :Param Phone_Number varchar(50)
        :Param Hours_Of_Operation datetime
        :Param reviews string
        :return: True
        :raises: Database errors on connection and insertion
        """
        pass
