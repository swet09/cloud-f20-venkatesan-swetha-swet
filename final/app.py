from datetime import datetime
import logging
import os
import base64
from flask import Flask, redirect, render_template, request
from io import BytesIO
from google.cloud import datastore
from google.cloud import storage
import random
from google.cloud import vision
import string
import argparse
import six
from googleapiclient.discovery import build
import json

#pass gcs bucket name
CLOUD_STORAGE_BUCKET = "ml_test_swet"
my_cse_id = "<passyour cse ID>"
dev_key = <"passyour API_KEY">

app = Flask(__name__)


def translate_text(text):
    # [START translate_translate_text]
    """Translates text into the target language.
    Target must be an ISO 639-1 language code.
    See https://g.co/cloud/translate/v2/translate-reference#supported_languages
    """
    from google.cloud import translate_v2 as translate

    translate_client = translate.Client()

    if isinstance(text, six.binary_type):
        text = text.decode("utf-8")

    # Text can also be a sequence of strings, in which case this method  will return a sequence of results for each text.
    result = translate_client.translate(text, target_language='en')

    return result["translatedText"]

"""using discovery service to pass entities extracted using visian and translate functions to be be passed in search engine built
     results can be pint in the fomat like titles,links,snippet based on enities we are passing
     In this case we are passing suggested web matches buest guess and we re printing top 10 links in fianl out put
     """
def google_search(search_term, cse_id, **kwargs):
    my_cse_id = "b6dd0e8a111b63fa3"
    dev_key = "AIzaSyDKHFrKqEVXVwKhdOTmYocmJOc8CqEnx5s"
    service = build("customsearch", "v1", developerKey=dev_key)
    res = service.cse().list(q=search_term, cx=cse_id, lr='lang_en', hl='lang_en').execute()
    if res and int(res['searchInformation']['totalResults']) > 0:
        result = res['items'][0]
        return str(result['link'])

    else:
        return False




@app.route('/')
def homepage():
    # Create a Cloud Datastore client.
    datastore_client = datastore.Client()

    # Use the Cloud Datastore client to fetch information from Datastore about  each photo.
    query = datastore_client.query(kind='Faces')
    image_entities = list(query.fetch())

    # Return a Jinja2 HTML template and pass in image_entities as a parameter.
    return render_template('homepage.html', image_entities=image_entities)


@app.route('/upload_photo', methods=['GET', 'POST'])
def upload_photo():
 
    photo = request.files['file']
    
    random1 = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(5)])
    new_path = random1 + photo.filename
    # Create a Cloud Storage client.
    storage_client = storage.Client()

    # Get the bucket that the file will be uploaded to.
    bucket = storage_client.bucket("ml_test_swet")

    # Create a new blob and upload the file's content.
    blob = bucket.blob(new_path)
    blob.upload_from_string(
            photo.read(), content_type=photo.content_type)

    # Make the blob publicly viewable.
    blob.make_public()


    # Create a Cloud Vision client.
    vision_client = vision.ImageAnnotatorClient()

    # Use the Cloud Vision client to detect a face for our image.
    source_uri = 'gs://{}/{}'.format(CLOUD_STORAGE_BUCKET, blob.name)
    image = vision.Image(
        source=vision.ImageSource(gcs_image_uri=source_uri))
    #image = vision.Image()
    faces = vision_client.face_detection(image).face_annotations
     
    response = vision_client.label_detection(image=image)
    #detect all labels
    labels = response.label_annotations
    label = ""
  
    if labels:
        if len(labels) < 5:
            for j in labels:
                label = label + ", " + j.description
        else:
            for i in range(5):
                label = label + ", " + labels[i].description
        label = label[1:]
    else:
        label = "No labels found" 



    #detcet all logos
    response = vision_client.logo_detection(image=image)
    logos = response.logo_annotations
    logo = ""
    if len(logos) > 0:
        for i in logos:
    
            logo = logo + ", " + i.description
        logo = logo[1:]
    else:
        logo = "No Logos Found"

   # based on image will find closest web searches guess
   # these labels are passed to google search for discovery functionality
    response = vision_client.web_detection(image=image)
    annotations = response.web_detection

    label_web_guess = ""
    googled_label_web_guess = ""
    length_of = len(annotations.web_entities)
    if annotations.web_entities:
        if len(annotations.web_entities) < 5:
            for j in annotations.web_entities:
                label_web_guess = label_web_guess + ", " + j.description
                search = google_search(j.description , my_cse_id)
                if search:
                    googled_label_web_guess = googled_label_web_guess + " " + search
        else:
            for i in range(5):
                label_web_guess = label_web_guess + ", " + annotations.web_entities[i].description
                search = google_search(annotations.web_entities[i].description , my_cse_id)
                if search:
                    googled_label_web_guess = googled_label_web_guess + " " + search
        label_web_guess = label_web_guess[1:]
    else:
        label_web_guess = "No web results found" 


    url_similar_image = ""
    if annotations.visually_similar_images:
        url_similar_image = annotations.visually_similar_images[0].url

    else:
        url_similar_image = ""

    
    # If a face is detected, save to Datastore the likelihood that the face
    # displays 'joy,' as determined by Google's Machine Learning algorithm.
    if len(faces) > 0:
        face = faces[0]

        # Convert the likelihood string.
        likelihoods = [
            'Unknown', 'Very Unlikely', 'Unlikely', 'Possible', 'Likely',
            'Very Likely']
        face_joy = likelihoods[face.joy_likelihood]
    else:
        face_joy = 'Unknown'


    response = vision_client.text_detection(image=image)
    
    # Limit to 50
    # see if any texts of any languages are found in the image uploaded and if any texts are found they are passed in to google search else labels web guess are passes
    texts = response.text_annotations[:10]
    text_lifted = ""
    translated_text = ""
    googled_search = ""
    if texts:

        for text in texts:
            # print('\n"{}"'.format(text.description))
            entity_annotation = text 
            description_string = str(entity_annotation.description)
            text_lifted = text_lifted + " " + text.description
            translated = translate_text(description_string)
            translated_text = translated_text + " " + translated
          #  print("Translated text: " + translated_text)
            
            search = google_search(translated , my_cse_id ,excludeFromIndexes=True)
            if search:
                googled_search = googled_search + " " + search
    else:
        text_lifted = "No Text Found"


#    print(f"label_search: {googled_label_web_guess}, image_search:  {googled_search}")
    if not googled_search or googled_search.isspace():
        googled_search = googled_label_web_guess


    # Create a Cloud Datastore client.
    datastore_client = datastore.Client()

    # Fetch the current date / time.
    current_datetime = datetime.now()

    # The kind for the new entity.
    kind = 'Faces'

    # The name/ID for the new entity.
    name = blob.name

    # Create the Cloud Datastore key for the new entity.
    key = datastore_client.key(kind, name)

    # Construct the new entity using the key. Set dictionary values for entity
    # keys blob_name, storage_public_url, timestamp, and joy.
    #passing enitites and after various test added exclude_from_indexes['for required entities'] to hande sze more than 1500 bytes
    entity = datastore.Entity(key,exclude_from_indexes=['googled_search','text', 'translated_text'])
    entity['blob_name'] = blob.name
    entity['image_public_url'] = blob.public_url
    entity['timestamp'] = current_datetime
    entity['joy'] = face_joy
    entity['label'] = label
    entity['logos_detected'] = logo
    entity['best_guess'] = label_web_guess
    entity['number_found'] = length_of
    entity['text'] = text_lifted
    entity['translated_text'] = translated_text
    entity['googled_search'] = googled_search
    entity['image_url'] = url_similar_image


    # Save the new entity to Datastore.
    datastore_client.put(entity)

    # Redirect to the home page.

    return redirect('/')


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. 
    app.run(host='0.0.0.0', port=5000, debug=True)

