"""
this file call all other python file index,sign,listing and theres also configuration for the port to list on 5000
Social services flask app.
"""
import flask
from flask.views import MethodView
from index import Index
from sign import Sign
from listing import listing
app = flask.Flask(__name__)       # our Flask app

app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

app.add_url_rule('/sign/',
                 view_func=Sign.as_view('sign'),
                 methods=['GET', 'POST'])

app.add_url_rule('/listing/',
                 view_func=listing.as_view('listing'),
                 methods=["GET"])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
