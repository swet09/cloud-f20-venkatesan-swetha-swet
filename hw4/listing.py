# this python file calls listing.html file and this is invoked in app.py file and filnally listing page is seen through this
from flask import redirect, request, url_for, render_template
from flask.views import MethodView
from datetime import datetime
import gbmodel

class listing(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(Name=row[0], Description=row[1], Street_Address=row[2], Type_Of_Service=row[3], Phone_Number=row[4], Hours_Of_Operation=row[5], Reviews=row[6]) for row in model.select()]
        return render_template('listing.html',entries=entries)
 
