"""
Python list model
"""
from datetime import datetime
from .Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = []

    def select(self):
        """
        Returns services list of lists
        Each list in services contains: name, Description, Street_Address, Type_Of_Service, Phone_Number, Hours_Of_Operation, reviews
        :return: List of lists
        """
        return self.guestentries

    def insert(self, Name, Description, Street_Address, Type_Of_Service, Phone_Number, Hours_Of_Operation, Reviews):
        """
        Appends a new list of values representing new message into guestentries
        :param Name: String
        :param Description: String
        :param Street_Address: String
        :Param Type_Of_Service: string
        :Param Phone_Number  varchar(50)
        :Param Hours_Of_Operation datetime
        :Param reviews string
        :return: True
        """
        params = [Name, Description, Street_Address, Type_Of_Service, Phone_Number, Hours_Of_Operation, Reviews]
        self.guestentries.append(params)
        return True
