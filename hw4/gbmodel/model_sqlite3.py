"""
Social Service Events In Portland  flask app.
SQLite database that looks something like the following:

+-----------------+----------------------+----------------+------------------------+----------------------+-------------------+--------------------------------------------------------+
| Name            | Description          | Street Addres  | Type of Service        |Phone Number          |Hours Of Operation |  Reviews                                               |
+=================+======================+================+========================+======================+===================+========================================================+
| Child Foundation|help children in need | 2020 NE        |Feeding needy children  |503-224-0409          |9am-9pm            |  You have touched lives of so many children            |
+-----------------+----------------------+----------------+------------------------+----------------------+-------------------+--------------------------------------------------------+

This can be created with the following SQL (see bottom of this file):

    create table services (name text, Description text, street address text,type of service  text,phone number text,Hours of operation time,reviews text);

"""
from datetime import datetime
from .Model import Model
import sqlite3
DB_FILE = 'services.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from services")
        except sqlite3.OperationalError:
            cursor.execute("create table services (Name text, Description text, Street_Address text,Type_Of_Service text,Phone_Number text,Hours_Of_Operation datetime,Reviews text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, description, street address, type of service,phone number,Hours of operation,reviews
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM services")
        return cursor.fetchall()

    def insert(self, Name, Description, Street_Address, Type_Of_Service, Phone_Number, Hours_Of_Operation, Reviews):
        """
        Inserts entry into database
        :param Name: String
        :param Description: String
        :param Street_Address: String
        :Param Type_Of_Service: string
        :Param Phone_Number varchar(50)
        :Param Hours_Of_Operation datetime
        :Param reviews string 
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'Name':Name, 'Description':Description, 'Street_Address':Street_Address, 'Type_Of_Service':Type_Of_Service, 'Phone_Number':Phone_Number , 'Hours_Of_Operation':Hours_Of_Operation , 'Reviews':Reviews }
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into services (Name, Description, Street_Address, Type_Of_Service, Phone_Number, Hours_Of_Operation, Reviews) VALUES (:Name, :Description, :Street_Address, :Type_Of_Service, :Phone_Number, :Hours_Of_Operation, :Reviews)", params)

        connection.commit()
        cursor.close()
        return True
